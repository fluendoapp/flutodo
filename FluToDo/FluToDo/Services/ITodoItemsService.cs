﻿using FluToDo.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace FluToDo.Services
{
    public interface ITodoItemsService
    {
        Task<ObservableCollection<TodoItem>> GetTodoItemsAsync();

        Task<bool> CreateTodoItemAsync(TodoItem todoItem);

        Task<bool> UpdateTodoItemAsync(TodoItem todoItem);

        Task<bool> DeleteTodoItemAsync(TodoItem todoItem);
    }
}