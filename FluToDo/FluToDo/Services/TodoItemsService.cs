﻿using FluToDo.Models;
using Newtonsoft.Json;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace FluToDo.Services
{
    public class TodoItemsService : RestServiceBase, ITodoItemsService
    {
        #region Fields

        // Just to have some data to see since I cannot access to the service and the call always fails.
        private List<TodoItem> _internalDebugData;

        #endregion Fields

        #region Constructors

        public TodoItemsService(IPageDialogService pageDialogService) : base(pageDialogService)
        {
            // Just to have some data to see since the calls to the service will never return data.
            _internalDebugData = new List<TodoItem>
            {
                new TodoItem() { Key = "1", Name = "Task one", IsComplete = true },
                new TodoItem() { Key = "2", Name = "Task two", IsComplete = false },
                new TodoItem() { Key = "3", Name = "Task three", IsComplete = true },
                new TodoItem() { Key = "4", Name = "Task four", IsComplete = false },
                new TodoItem() { Key = "5", Name = "Task five", IsComplete = false }
            };
        }

        #endregion Constructors

        #region Methods

        public async Task<ObservableCollection<TodoItem>> GetTodoItemsAsync()
        {
            try
            {
                string data = await GetDataAsync(new Uri(AppConfig.TodoAPIBaseUri));
                if (!string.IsNullOrEmpty(data))
                {
                    IEnumerable<TodoItem> items = JsonConvert.DeserializeObject<IEnumerable<TodoItem>>(data);
                    if (items != null)
                        return new ObservableCollection<TodoItem>(items);
                }
            }
            catch (Exception)
            {
                await _pageDialogService.DisplayAlertAsync(GlobalResources.ErrorMessageTitle, "an error occurred communicating with the server, please try again.", GlobalResources.ErrorMessageCancelButton);
            }

            // Return an ObservableCollection with the data of _internalDebugData to have some data to see, since I cannot access to the service and the call always fails.
            return new ObservableCollection<TodoItem>(_internalDebugData);
        }

        public async Task<bool> CreateTodoItemAsync(TodoItem todoItem)
        {
            if (todoItem == null)
                return false;

            bool result = await PostDataAsync(new Uri(AppConfig.TodoAPIBaseUri), todoItem);
            if (!result) // Since I cannot access to the service and the call always fails, I will create a new item on the _internalDebugData.
            {
                todoItem.Key = (_internalDebugData.Count + 1).ToString();

                _internalDebugData.Add(todoItem);

                return true;
            }

            return result;
        }

        public async Task<bool> DeleteTodoItemAsync(TodoItem todoItem)
        {
            if (todoItem == null)
                return false;

            bool result = await DeleteDataAsync(new Uri(AppConfig.TodoAPIBaseUri), todoItem.Key);
            if (!result) // Since I cannot access to the service and the call always fails, I will delete the Todo item on the _internalDebugData.
            {
                TodoItem itemToRemove = _internalDebugData.SingleOrDefault(ti => ti.Key == todoItem.Key);
                if (itemToRemove != null)
                {
                    _internalDebugData.Remove(itemToRemove);
                }
                else
                {
                    return false;
                }

                return true;
            }

            return result;
        }

        public async Task<bool> UpdateTodoItemAsync(TodoItem todoItem)
        {
            if (todoItem == null)
                return false;

            bool result = await PutDataAsync(new Uri(AppConfig.TodoAPIBaseUri), todoItem);
            if (!result) // Since I cannot access to the service and the call always fails, I will update the Todo item on the _internalDebugData.
            {
                TodoItem itemToUpdate = _internalDebugData.SingleOrDefault(ti => ti.Key == todoItem.Key);
                if (itemToUpdate != null)
                {
                    itemToUpdate.IsComplete = todoItem.IsComplete;
                }
                else
                {
                    return false;
                }

                return true;
            }

            return result;
        }

        #endregion Methods
    }
}