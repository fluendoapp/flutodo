﻿using Newtonsoft.Json;
using Prism.Services;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FluToDo.Services
{
    /// <summary>
    /// I could used refit or other nuget package to perform the REST service calls easily, but since I cannot access to the service and the call always fails,
    /// it's only to show the calls and data manipulation.
    /// </summary>
    public abstract class RestServiceBase
    {
        #region Constants

        private const string JsonMediaType = "application/json";

        #endregion Constants

        #region Fields

        private HttpClient _client;

        protected IPageDialogService _pageDialogService;

        #endregion Fields

        #region Constructors

        public RestServiceBase(IPageDialogService pageDialogService)
        {
            _pageDialogService = pageDialogService;

            _client = new HttpClient();
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Perform an Async get.
        /// </summary>
        /// <param name="address">The Uri to perform the get.</param>
        /// <returns>Returns the string response if the get is successful or null.</returns>
        protected async Task<string> GetDataAsync(Uri address)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));

            try
            {
                HttpResponseMessage response = await _client.GetAsync(address);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }
            }
            catch
            {
                // In case of error will have the same behavior of IsSuccessStatusCode = false
            }

            return null;
        }

        /// <summary>
        /// Perform an Async post.
        /// </summary>
        /// <typeparam name="T">Type of the data to send.</typeparam>
        /// <param name="address">The Uri to perform the get.</param>
        /// <param name="data">Object to send.</param>
        /// <returns>Returns if post was successful.</returns>
        protected async Task<bool> PostDataAsync<T>(Uri address, T data)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            try
            {
                StringContent content = SerializeDataToJson(data);

                HttpResponseMessage response = response = await _client.PostAsync(address, content);

                return response.IsSuccessStatusCode;
            }
            catch
            {
                // In case of error will have the same behavior of IsSuccessStatusCode = false
            }

            return false;
        }

        /// <summary>
        /// Perform an Async put.
        /// </summary>
        /// <typeparam name="T">Type of the data to update.</typeparam>
        /// <param name="address">The Uri to perform the get.</param>
        /// <param name="data">Object to update.</param>
        /// <returns>Returns if put was successful.</returns>
        protected async Task<bool> PutDataAsync<T>(Uri address, T data)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            try
            {
                StringContent content = SerializeDataToJson(data);

                HttpResponseMessage response = await _client.PutAsync(address, content);

                return response.IsSuccessStatusCode;
            }
            catch
            {
                // In case of error will have the same behavior of IsSuccessStatusCode = false
            }

            return false;
        }

        /// <summary>
        /// Perform an Async delete.
        /// </summary>
        /// <param name="address">The Uri to perform the delete.</param>
        /// <param name="id">The id of the object to delete.</param>
        /// <returns>Returns if delete was successful.</returns>
        protected async Task<bool> DeleteDataAsync(Uri address, string id)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address));
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException(nameof(id));

            try
            {
                Uri uri = new Uri(string.Format(address.AbsolutePath, id));

                HttpResponseMessage response = await _client.DeleteAsync(uri);

                return response.IsSuccessStatusCode;
            }
            catch
            {
                // In case of error will have the same behavior of IsSuccessStatusCode = false
            }

            return false;
        }

        #region Private Methods

        private static StringContent SerializeDataToJson<T>(T data)
        {
            var json = JsonConvert.SerializeObject(data);
            var content = new StringContent(json, Encoding.UTF8, JsonMediaType);
            return content;
        }

        #endregion Private Methods

        #endregion Methods
    }
}