﻿namespace FluToDo
{
    internal static class GlobalResources
    {
        #region Constants

        public const string ErrorMessageTitle = "Error";
        public const string SuccessMessageTitle = "Success";
        public const string ErrorMessageCancelButton = "Close";
        public const string SuccessMessageCancelButton = "Ok";

        #endregion Constants
    }
}