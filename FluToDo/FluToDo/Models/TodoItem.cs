﻿using Prism.Mvvm;

namespace FluToDo.Models
{
    public class TodoItem : BindableBase
    {
        #region Properties

        private string _key;
        public string Key
        {
            get { return _key; }
            set { SetProperty(ref _key, value); }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private bool _isComplete;
        public bool IsComplete
        {
            get { return _isComplete; }
            set { SetProperty(ref _isComplete, value); }
        }

        #endregion Properties

        #region Methods

        public TodoItem Clone()
        {
            return new TodoItem() { Key = Key, Name = Name, IsComplete = IsComplete };
        }

        #endregion Methods
    }
}