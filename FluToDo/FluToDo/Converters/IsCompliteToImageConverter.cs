﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace FluToDo.Converters
{
    public class IsCompliteToImageConverter : IValueConverter
    {
        #region Constants

        private const string CompletedImageName = "circleCheck.png";

        #endregion Constants

        #region Methods

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool && (bool)value)
            {
                return CompletedImageName;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion Methods
    }
}