﻿using FluToDo.Models;
using FluToDo.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace FluToDo.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        #region Constants

        private const string MainPageTitle = "FluToDo";
        private const string CreateNewTodoItemPageName = "CreateNewTodoItemPage";

        #endregion Constants

        #region Fields

        private ITodoItemsService _todoItemsService;

        #endregion Fields

        #region Properties

        private ObservableCollection<TodoItem> _todoItemList;
        public ObservableCollection<TodoItem> TodoItemList
        {
            get { return _todoItemList; }
            set { SetProperty(ref _todoItemList, value); }
        }

        private TodoItem _selectedTodoItem;
        public TodoItem SelectedTodoItem
        {
            get { return _selectedTodoItem; }
            set { SetProperty(ref _selectedTodoItem, value); }
        }

        private bool _isTodoItemListRefreshing;
        public bool IsTodoItemListRefreshing
        {
            get { return _isTodoItemListRefreshing; }
            set { SetProperty(ref _isTodoItemListRefreshing, value); }
        }

        public DelegateCommand CreateTodoItemCommand { get; set; }

        public DelegateCommand<TodoItem> DeleteTodoItemCommand { get; set; }

        public DelegateCommand ChangeTodoItemCompletionCommand { get; set; }

        public DelegateCommand RefreshTodoItemListCommand { get; set; }

        #endregion Properties

        #region Constructors

        public MainPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ITodoItemsService todoItemsService) : base(navigationService, pageDialogService)
        {
            _todoItemsService = todoItemsService;

            Title = MainPageTitle;

            CreateTodoItemCommand = new DelegateCommand(CreateNewTodoItemCommandHandler);
            DeleteTodoItemCommand = new DelegateCommand<TodoItem>(DeleteTodoItemCommandHandler);
            ChangeTodoItemCompletionCommand = new DelegateCommand(ChangeTodoItemCompletionCommandHandler);
            RefreshTodoItemListCommand = new DelegateCommand(RefreshTodoItemListCommandHandler, CanRefreshTodoItemListCommandHandler);
        }

        #endregion Constructors

        #region Methods

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);

            await LoadData();
        }

        #region Private Methods

        private async Task LoadData(bool isRefreshing = false)
        {
            // Just to show the refresh working as expected since I cannot access to the service.
            if (isRefreshing)
            {
                await _todoItemsService.CreateTodoItemAsync(new TodoItem() { Key = (TodoItemList.Count + 1).ToString(), Name = "Refreshing todo item", IsComplete = false });
            }

            TodoItemList = await _todoItemsService.GetTodoItemsAsync();
        }

        private async void CreateNewTodoItemCommandHandler()
        {
            await _navigationService.NavigateAsync(CreateNewTodoItemPageName);
        }

        private async void DeleteTodoItemCommandHandler(TodoItem todoItem)
        {
            if (await _todoItemsService.DeleteTodoItemAsync(todoItem))
            {
                await _pageDialogService.DisplayAlertAsync(GlobalResources.SuccessMessageTitle, $"ToDo item {todoItem.Name} has been deleted correctly", GlobalResources.SuccessMessageCancelButton);

                // Remove deleted TodoItem from the TodoItemList
                TodoItemList.Remove(todoItem);
            }
            else
            {
                await _pageDialogService.DisplayAlertAsync(GlobalResources.ErrorMessageTitle, $"An error occurred deleting the Todo item {todoItem.Name}, please try again.", GlobalResources.ErrorMessageCancelButton);
            }
        }

        private async void ChangeTodoItemCompletionCommandHandler()
        {
            // Clone TodoItem to not update local data, otherwise the SelectedItem would update immediately in the interface (because of binding)
            // and lead to inconsistent data in case of update fails.
            TodoItem todoItemToChange = SelectedTodoItem.Clone();
            todoItemToChange.IsComplete = !todoItemToChange.IsComplete;

            // If correctly update then update the local info.
            if (await _todoItemsService.UpdateTodoItemAsync(todoItemToChange))
            {
                SelectedTodoItem.IsComplete = todoItemToChange.IsComplete;
            }
            else
            {
                await _pageDialogService.DisplayAlertAsync(GlobalResources.ErrorMessageTitle, $"An error occurred updating completion of Todo item {SelectedTodoItem.Name}, please try again.", GlobalResources.ErrorMessageCancelButton);
            }

            // To clear the selection
            SelectedTodoItem = null;
        }

        private async void RefreshTodoItemListCommandHandler()
        {
            await LoadData(true);

            // To inform the ListView that the refresh is complete.
            IsTodoItemListRefreshing = false;
        }

        private bool CanRefreshTodoItemListCommandHandler()
        {
            return !IsTodoItemListRefreshing;
        }

        #endregion Private Methods

        #endregion Methods
    }
}