﻿using FluToDo.Models;
using FluToDo.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;

namespace FluToDo.ViewModels
{
    public class CreateNewTodoItemPageViewModel : ViewModelBase
    {
        #region Constants

        private const string CreateNewTodoItemPageTitle = "New Todo";

        #endregion Constants

        #region Fields

        private ITodoItemsService _todoItemsService;

        #endregion Fields

        #region Properties

        private TodoItem _newTodoItem;
        public TodoItem NewTodoItem
        {
            get { return _newTodoItem; }
            set { SetProperty(ref _newTodoItem, value); }
        }

        public DelegateCommand CreateCommand { get; set; }

        #endregion Properties

        #region Constructors

        public CreateNewTodoItemPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, ITodoItemsService todoItemsService) : base(navigationService, pageDialogService)
        {
            _todoItemsService = todoItemsService;

            NewTodoItem = new TodoItem();
            Title = CreateNewTodoItemPageTitle;

            CreateCommand = new DelegateCommand(CreateCommandCommandHandler);
        }

        #endregion Constructors

        #region Methods

        #region Private Methods

        private async void CreateCommandCommandHandler()
        {
            if (string.IsNullOrEmpty(NewTodoItem.Name))
            {
                await _pageDialogService.DisplayAlertAsync(GlobalResources.ErrorMessageTitle, "Please specify a name first.", GlobalResources.ErrorMessageCancelButton);
            }
            else
            {
                if (await _todoItemsService.CreateTodoItemAsync(NewTodoItem))
                {
                    await _pageDialogService.DisplayAlertAsync(GlobalResources.SuccessMessageTitle, "New Todo item created.", GlobalResources.SuccessMessageCancelButton);

                    await _navigationService.GoBackAsync();
                }
                else
                {
                    await _pageDialogService.DisplayAlertAsync(GlobalResources.ErrorMessageTitle, "An error occurred creating the Todo item, please try again.", GlobalResources.ErrorMessageCancelButton);
                }
            }
        }

        #endregion Private Methods

        #endregion Methods
    }
}