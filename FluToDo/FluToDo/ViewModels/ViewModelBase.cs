﻿using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;

namespace FluToDo.ViewModels
{
    public class ViewModelBase : BindableBase, INavigationAware, IDestructible
    {
        #region Fields

        protected INavigationService _navigationService;
        protected IPageDialogService _pageDialogService;

        #endregion Fields

        #region Properties

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        #endregion Properties

        #region Constructors

        public ViewModelBase(INavigationService navigationService, IPageDialogService pageDialogService)
        {
            _navigationService = navigationService;
            _pageDialogService = pageDialogService;
        }

        #endregion Constructors

        #region Methods

        public virtual void OnNavigatedFrom(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatedTo(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatingTo(NavigationParameters parameters)
        {
        }

        public virtual void OnNavigatingToAsync(NavigationParameters parameters)
        {
        }

        public virtual void Destroy()
        {
        }

        #endregion Methods
    }
}